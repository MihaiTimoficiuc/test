import React from 'react';

import './Card.scss';

function Card({
  title = 'Cabin',
  image = 'https://images.pexels.com/photos/673788/pexels-photo-673788.jpeg?cs=srgb&dl=pexels-mateas-petru-673788.jpg&fm=jpg',
  desc = 'Entire cabine just for you',
  beds = 1,
  baths = 1,
  price = 168,
}) {
  return (
    <div className="card">
      <div
        className="card__img"
        style={{ backgroundImage: `url(${image})` }}
      >
        <p className="text text--sm card__img-text">4.6</p>
      </div>
      <div className="card__body">
        <h1 className="title title--sm card__title">
          {title}
        </h1>
        <p className="text text--sm card__desc">{desc}</p>
        <div className="card__info">
          <p className="text text--sm">{beds} beds</p>
          <p className="text text--sm">{baths} bath</p>
        </div>
        <p className="text text--sm">${price} / day</p>
      </div>
    </div>
  );
}

export default Card;
