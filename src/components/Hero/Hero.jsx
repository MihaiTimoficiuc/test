import React, {
  useEffect,
  useReducer,
  useState,
} from 'react';

import { BiSearch } from 'react-icons/bi';

import './Hero.scss';

const initialState = {
  cabin: 'A1',
  checkIn: '',
  checkOut: '',
  guests: 2,
};

function reducer(state, action) {
  switch (action.type) {
    case 'HANDLE_CHANGE':
      return {
        ...state,
        [action.payload.name]: action.payload.value,
      };
    case 'HANDLE_SUBMIT':
      return {
        cabin: 'A1',
        checkIn: '',
        checkOut: '',
        guests: 2,
      };
    default:
      throw new Error();
  }
}

function Hero() {
  const [state, dispatch] = useReducer(
    reducer,
    initialState
  );

  const [reservations, setReservations] = useState([]);

  const [validReservation, setValidReservation] =
    useState(true);

  useEffect(() => {
    console.log(state);
  }, [state]);

  useEffect(() => {
    console.log(reservations);
    reservations.forEach((reservation) => {
      console.log(reservation.cabin);
      console.log('State cabin', state.cabin);
    });
  }, [reservations]);

  useEffect(() => {
    const getReservations = async () => {
      const data = await fetch('/data/reservations.json', {
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
      });
      const res = await data.json();
      setReservations(res.reservations);
    };
    getReservations();
  }, []);

  const reservationValidation = () => {
    reservations.forEach((reservation) => {
      if (state.cabin === reservation.cabin)
        setValidReservation(false);
      if (state.checkIn === reservation.checkIn)
        setValidReservation(false);
      else setValidReservation(true);
    });
  };

  return (
    <div className="hero">
      <div className="hero__text">
        <h1 className="title title--md">
          Reconnect with nature
        </h1>
        <p className="text text--lg">
          Curated spots to relax
        </p>
      </div>
      <div className="hero__inputs">
        <div className="container container--cl hero__input">
          <p className="text text--sm hero__input-text ">
            Cabin
          </p>
          <input
            className="input"
            type="text"
            value={state.cabin}
            name="cabin"
            onChange={(e) =>
              dispatch({
                type: 'HANDLE_CHANGE',
                payload: {
                  name: e.target.name,
                  value: e.target.value,
                },
              })
            }
            placeholder="Add a location"
          />
        </div>
        <div className="container container--cl hero__input">
          <p className="text text--sm hero__input-text">
            Check in
          </p>
          <input
            className="input"
            type="date"
            value={state.checkIn}
            name="checkIn"
            onChange={(e) => {
              if (
                state.checkOut >= e.target.value ||
                state.checkOut == ''
              )
                dispatch({
                  type: 'HANDLE_CHANGE',
                  payload: {
                    name: e.target.name,
                    value: e.target.value,
                  },
                });
              else
                alert(
                  'Please enter a check in date that is valid'
                );
            }}
            placeholder="Add dates"
          />
        </div>
        <div className="container container--cl hero__input">
          <p className="text text--sm hero__input-text">
            Check out
          </p>
          <input
            className="input"
            type="date"
            value={state.checkOut}
            name="checkOut"
            onChange={(e) => {
              if (
                state.checkIn <= e.target.value ||
                state.checkIn == ''
              )
                dispatch({
                  type: 'HANDLE_CHANGE',
                  payload: {
                    name: e.target.name,
                    value: e.target.value,
                  },
                });
              else
                alert(
                  'Please enter a check out date that is valid'
                );
            }}
            placeholder="Add dates"
          />
        </div>
        <div className="container container--cl hero__input">
          <p className="text text--sm hero__input-text">
            Guests
          </p>
          <input
            className="input"
            type="number"
            value={state.guests}
            name="guests"
            onChange={(e) => {
              if (
                e.target.value > 0 ||
                e.target.value === ''
              ) {
                dispatch({
                  type: 'HANDLE_CHANGE',
                  payload: {
                    name: e.target.name,
                    value: e.target.value,
                  },
                });
              } else
                alert('Please enter a valid guest number');
            }}
            placeholder="Add guests"
          />
        </div>
        <div
          onClick={() => {
            reservationValidation();
            if (validReservation && reservations != []) {
              dispatch({
                type: 'HANDLE_SUBMIT',
              });
            } else alert('invalid reservation');
          }}
          className="hero__inputs-submit"
        >
          <BiSearch size={30} color={'#00000'} />
        </div>
      </div>
      <div className="hero__search">
        <input
          type="text"
          className="input input--border-round hero__search-input"
          placeholder="Where are you going?"
        />
      </div>
    </div>
  );
}

export default Hero;
