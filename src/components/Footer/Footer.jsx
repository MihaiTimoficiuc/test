import React from 'react';
import { Link } from 'react-router-dom';
import Social from '../Social/Social';

import './Footer.scss';

function Footer() {
  return (
    <div className="footer">
      <p className="text text--sm">&copy; 2022 Copyright</p>
      <p className="text text--sm">
        <Link to={'/'}>Privacy</Link>
      </p>
      <p className="text text--sm">
        <Link to={'/'}>Terms</Link>
      </p>
      <p className="text text--sm">
        <Link to={'/'}>Sitemap</Link>
      </p>
      <Social />
    </div>
  );
}

export default Footer;
