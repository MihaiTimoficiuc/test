import React from 'react';
import { BsFacebook } from 'react-icons/bs';
import { GrInstagram } from 'react-icons/gr';
import { BsPinterest } from 'react-icons/bs';

import './Social.scss';

function Social() {
  return (
    <div className="social">
      <BsFacebook size={20} />
      <GrInstagram size={20} />
      <BsPinterest size={20} />
    </div>
  );
}

export default Social;
