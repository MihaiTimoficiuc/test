import React from 'react';

import './Contact.scss';

function Contact() {
  return (
    <div className="contact">
      <h1 className="title title--md contact__title">
        Contact us
      </h1>
      <div className="contact__info">
        <div className="contact__info-items">
          <div className="contact__info-item">
            <p className="text text--md">Phone:</p>
            <p className="text text--md">0721 213 897</p>
          </div>
          <div className="contact__info-item">
            <p className="text text--md">Whatsapp:</p>
            <p className="text text--md">0721 213 897</p>
          </div>
          <div className="contact__info-item">
            <p className="text text--md">Email:</p>
            <p className="text text--md">
              support@reconnect.com
            </p>
          </div>
        </div>

        <div className="contact__map">
          <iframe
            width="400"
            height="400"
            id="gmap_canvas"
            src="https://maps.google.com/maps?q=2880%20Broadway,%20New%20York&t=&z=13&ie=UTF8&iwloc=&output=embed"
            frameBorder="0"
            scrolling="no"
            marginHeight="0"
            marginWidth="0"
          ></iframe>
        </div>
      </div>

      <h1 className="title title--md contact__title">
        Contact form
      </h1>

      <form className="contact__form">
        <label htmlFor="name">Name</label>
        <input
          className="contact__input"
          id="name"
          type="text"
        />
        <label htmlFor="email">Email</label>
        <input
          className="contact__input"
          id="email"
          type="text"
        />
        <label htmlFor="subject">Subject</label>
        <input
          className="contact__input"
          id="subject"
          type="text"
        />
        <textarea
          className="contact__textarea"
          cols="30"
          rows="10"
        ></textarea>
        <button className="button button--primary contact__button">
          Send
        </button>
      </form>
    </div>
  );
}

export default Contact;
