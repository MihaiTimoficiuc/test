import React from 'react';
import Card from '../Card/Card';

import './Cards.scss';

function Cards() {
  return (
    <div className="cards">
      <Card />
      <Card />
      <Card />
      <Card />
    </div>
  );
}

export default Cards;
