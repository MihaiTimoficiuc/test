import React from 'react';

import { MdMenu } from 'react-icons/md';
import { BiSearch, BiUser } from 'react-icons/bi';
import { Link } from 'react-router-dom';

import '../Header/Header.scss';

function Header() {
  return (
    <div className="header">
      <div className="container container--row container--centered">
        <MdMenu
          size={30}
          color={'#00000'}
          className="header__menu"
        />
        <h1 className="title title--sm">Reconnect</h1>
        <div className="header__links">
          <span>|</span>
          <Link to="">Rent</Link>
          <Link to="">Experiences</Link>
          <Link to="">Nearby</Link>
        </div>
      </div>

      <div className="container container--row container--centered">
        <BiSearch
          size={30}
          color={'#00000'}
          className="header__search"
        />
        <button className="button button--primary header__button">
          <BiUser size={20} />
          Sign in
        </button>
      </div>
    </div>
  );
}

export default Header;
