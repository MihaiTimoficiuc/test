import React from 'react';

import './styles/main.scss';
import Views from './routes/Views';
import Footer from './components/Footer/Footer';
import Header from './components/Header/Header';

function App() {
  return (
    <div className="app">
      <div className="app__main">
        <Header />
        <Views />
      </div>

      <Footer />
    </div>
  );
}

export default App;
