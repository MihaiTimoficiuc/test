import { Button } from 'react-bootstrap';
import React from 'react';
import Hero from '../components/Hero/Hero';
import Cards from '../components/Cards/Cards';
import Contact from '../components/Contact/Contact';

function Home() {
  return (
    <div>
      <Hero />
      <Cards />
      <Contact />
    </div>
  );
}

export default Home;
